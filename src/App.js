import React, {Component} from 'react';
import Radium, { StyleRoot } from 'radium';

import './App.scss';


import CardDisplay from './components/Card';

class App extends Component {
  state = {
    user_info : [
      {
        id: 1,
        status_list: [
          { id: 1, column: 5.68 },
          { id: 2, column: 5.68 },
          { id: 3, column: 29 },
          { id: 4, column: 192 },
          { id: 5, column: 32813.36 },
          { id: 6, column: 100 },
          { id: 7, column: 9.3 },
          { id: 8, column: 9.3 },
          { id: 9, column: 9.3 },
          { id: 10, column: 9.3 },
        ],
        user_pic: 'user_pic.png',
        user_flag: 'flag.png',
        user_code: 'B078WLH42J',
        user_title: 'Toddler Fishing Game Gifs for 2 3 Year Old Girl and the'
      }
    ],
  }

  numberWithCommas = (x) => {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  truncateString = (str, num) => {
      // Clear out that junk in your trunk
      if(str.length<=num){
          return str;
      }
      
      var strSliced = str.slice(0,num);
      
      if(strSliced.length<=3){
          return strSliced + "...";
      }
      
      return strSliced.slice(0,-3) + " ...";
  }

  render () {
    let card_display = '';

    if ( this.state.user_info ) {
      card_display = (
        <React.Fragment>
          {this.state.user_info.map(user => {

            return <CardDisplay 
                    key={user.id}
                    details={user} 
                    truncate={this.truncateString}
                    format={this.numberWithCommas} />;

          })}
        </React.Fragment>
      );
    }


    return (
      <StyleRoot>
        <div className="App">
          {card_display}
        </div>
      </StyleRoot>
    )
  }
}

export default Radium(App);
