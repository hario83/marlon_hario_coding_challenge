import React from 'react';

import Status from './CardContent/Status';
import UserDetails from './CardContent/UserDetails';

import 'bootstrap/dist/css/bootstrap.min.css';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import { 
    Container, 
    Row, 
    Col 
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
    faCaretDown,
    faBellSlash, 
    faFolderPlus, 
    faEllipsisV
} from '@fortawesome/free-solid-svg-icons';


const card = (props) => {
    const user_pics = props.details.user_pic; 

    return (
        <React.Fragment>
            <Container className="pt-5">
                <Row>
                    <Col md={12} className="px-0">
                        <Card>
                            <Card.Body className="py-3">
                                <Row className="d-flex justify-content-arround">
                                    <Col>
                                        <Row className="card_content">
                                            <Col className="card_content_1" xs={6} md={5} sm={6}>
                                                <Row>
                                                    <Col md={4} className="d-flex pr-0">
                                                        <Form className="align-self-center">
                                                            <Form.Group>
                                                                <Form.Check type="checkbox" />
                                                            </Form.Group>
                                                        </Form>
                                                        <img className="ml-3" alt="sample of one" width="64" src={require(`./../assets/images/${user_pics}`)}></img>
                                                    </Col>
                                                    <Col md={8}>
                                                        <UserDetails truncate_t={props.truncate} user_d={props.details} />
                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className="card_content_2 d-flex align-items-center px-3" xs={12} sm={12} md={7}>
                                                <Status status_d={props.details.status_list} format={props.format}/>
                                            </Col>
                                        </Row>
                                    </Col>

                                    <Col className="option_icon" md={1}>
                                        <ul className="d-flex flex-column align-items-center justify-content-around">
                                            <li><FontAwesomeIcon className="secondary" icon={faFolderPlus} /></li>
                                            <li><FontAwesomeIcon className="secondary" icon={faBellSlash} /></li>
                                            <li><FontAwesomeIcon className="secondary" icon={faEllipsisV} /></li>
                                        </ul>
                                    </Col>
                                </Row>
                            </Card.Body>
                            <FontAwesomeIcon className="secondary" size="2x" icon={faCaretDown} />
                        </Card>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    )
}

export default card;