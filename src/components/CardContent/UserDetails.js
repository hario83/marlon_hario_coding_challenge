import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import Media from 'react-bootstrap/Media';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { 
    faSkullCrossbones, 
    faLock, 
    faThList, 
    faBoxes, 
    faCommentDollar 
} from '@fortawesome/free-solid-svg-icons';


const userdetails = (props) => {
   const flag_path = props.user_d.user_flag;
   const user_title = props.user_d.user_title;
   const user_code = props.user_d.user_code;
   
    return (
        <React.Fragment>
            <Media>
                <Media.Body>
                    <p className="text-primary">
                        {props.truncate_t(user_title, 54)}
                    </p>
                    <ul className="d-flex justify-content-between align-items-center">
                        <li>
                            <img alt="sample of two" width="30" height="30" src={require(`./../../assets/images/${flag_path}`)}></img>
                        </li>
                        <li>
                            <small><b>{user_code}</b></small>
                        </li>
                        <li><FontAwesomeIcon className="text-danger" icon={faSkullCrossbones} /></li>
                        <li><FontAwesomeIcon className="text-danger" icon={faLock} /></li>
                        <li><FontAwesomeIcon className="secondary" icon={faThList} /></li>
                        <li><FontAwesomeIcon className="secondary" icon={faBoxes} /></li>
                        <li><FontAwesomeIcon className="secondary" icon={faCommentDollar} /></li>
                    </ul>
                </Media.Body>
            </Media>
        </React.Fragment>
    )
}

export default userdetails;