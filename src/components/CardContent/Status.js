import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

const status = (props) => {
    let status_display = '';

    if ( props.status_d ) {
        status_display = (
            <ul className="d-flex justify-content-between content_status">
                {props.status_d.map((stat, index) => {
                    let dSign = ''; 
                    let pSign = ''; 
                    let format_number = props.format(stat.column);
                    if ([0,1,4].includes(index)) {
                        dSign = '$';
                    } else if ([2,5].includes(index)) {
                        pSign = '%';
                    }
                    
                    return <li className="align-self-center" key={stat.id}>{dSign}{format_number}{pSign}</li>
                })}
            </ul>
        );
    }
    
   
    return (
        <React.Fragment>
            {status_display}
        </React.Fragment>
    )
}

export default status;